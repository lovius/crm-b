package com.happyGoCrm.controllerAdvice;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.happyGoCrm.common.HttpStatus;
import com.happyGoCrm.common.Result;
import com.happyGoCrm.exception.SqlConditionItemException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ExceptionAdvice{

	@ExceptionHandler(value = IllegalArgumentException.class)
	public Result<String> errorHandle(IllegalArgumentException ex){
		return new Result<String>(HttpStatus.exception,ex.getLocalizedMessage());	
	}
	@ExceptionHandler(value = Exception.class)
	public Result<String> errorHandle(Exception ex){
		log.info("ex = " + ex.getLocalizedMessage());
		log.info("ex = " + ex.getMessage());
		return new Result<String>(HttpStatus.exception,ex.getLocalizedMessage());	
	}
}
