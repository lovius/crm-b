package com.happyGoCrm.exception;

public class SqlConditionItemException extends Exception  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8903099036502528292L;
	
	public SqlConditionItemException() {
	    super();
	}
	
	public SqlConditionItemException(String s) {
	    super(s);
	}

}
