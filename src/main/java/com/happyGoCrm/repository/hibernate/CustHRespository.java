package com.happyGoCrm.repository.hibernate;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.happyGoCrm.model.entity.TbCust;
import com.happyGoCrm.model.entity.TbCustPK;

@Repository
public interface CustHRespository extends JpaRepository<TbCust, TbCustPK>, JpaSpecificationExecutor<TbCust>, QuerydslPredicateExecutor<TbCust>{

	@Query(nativeQuery = true, value = "select SEQ_CUST_ID.nextval as seq from dual")
	public long getTbCustSeq();
	
	@Query(nativeQuery = true, value = "select count(*) from tb_cust where PERSON_ID=:personId")
	public long checkPersonID(String personId);
	
}
