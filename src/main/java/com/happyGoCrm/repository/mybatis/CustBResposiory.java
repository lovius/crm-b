package com.happyGoCrm.repository.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.github.pagehelper.Page;
import com.happyGoCrm.model.entity.TbCust;
import com.happyGoCrm.model.queryForm.cust.CustForm;

@Mapper
public interface CustBResposiory {
	public long checkPersonID(String personId);
	
	public List<TbCust> getAll(CustForm custForm);

	public Page<TbCust> getPage(CustForm custForm);
}
