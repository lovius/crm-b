package com.happyGoCrm.service.cust;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.happyGoCrm.common.PageResult;
import com.happyGoCrm.model.dto.cust.CustDto;
import com.happyGoCrm.model.entity.TbCust;
import com.happyGoCrm.model.queryForm.cust.CustForm;
import com.happyGoCrm.service.CommonService;

public interface CustService extends CommonService<TbCust> {

	public List<TbCust> getList();

	public List<TbCust> getList(CustForm custForm);

	public Page<TbCust> getList(CustForm custForm, PageRequest pageRequest);

	public com.github.pagehelper.Page<TbCust> getPage(CustForm custForm);

	public Long getSequence();

	public Long checkPersonID(String personId);
	
	public PageResult<List<CustDto>> queryDsl(CustForm custForm,PageRequest pageRequest);
}
