package com.happyGoCrm.service.impl.cust;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.happyGoCrm.common.HttpStatus;
import com.happyGoCrm.common.PageObject;
import com.happyGoCrm.common.PageResult;
import com.happyGoCrm.model.dto.cust.CustDto;
import com.happyGoCrm.model.entity.QTbCust;
import com.happyGoCrm.model.entity.QTbRegion;
import com.happyGoCrm.model.entity.TbCust;
import com.happyGoCrm.model.entity.TbCustPK;
import com.happyGoCrm.model.queryForm.cust.CustForm;
import com.happyGoCrm.repository.hibernate.CustHRespository;
import com.happyGoCrm.repository.mybatis.CustBResposiory;
import com.happyGoCrm.service.cust.CustService;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustServiceImpl implements CustService {

	@Autowired
	private CustHRespository tbCustHRespository;

	@Autowired
	private CustBResposiory tbCustBRespository;

	@Transactional(readOnly = true)
	@Override
	public Long getSequence() {
		// TODO Auto-generated method stub
		return tbCustHRespository.getTbCustSeq();
	}

	@Transactional(readOnly = true)
	@Override
	public Long checkPersonID(String personId) {
		// TODO Auto-generated method stub
		return tbCustHRespository.checkPersonID(personId);
	}

	@Transactional(readOnly = true)
	@Override
	public List<TbCust> getList() {
		// TODO Auto-generated method stub
		return tbCustHRespository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public List<TbCust> getList(CustForm custForm) {
		// TODO Auto-generated method stub
		return tbCustBRespository.getAll(custForm);
	}

	@Override
	public void save(TbCust t) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(TbCust t) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<TbCust> getList(Map<String, Object> params, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public TbCust saveAndGetPk(TbCust t) throws Exception {
		// TODO Auto-generated method stub
		try {

			TbCustPK tbCustPK = t.getId();
			tbCustPK.setCustId(String.valueOf(this.getSequence()));
			t.setId(tbCustPK);

			return tbCustHRespository.saveAndFlush(t);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public Page<TbCust> getPage(CustForm custForm) {
		// TODO Auto-generated method stub
		return tbCustBRespository.getPage(custForm);
	}

	@Override
	public org.springframework.data.domain.Page<TbCust> getList(CustForm custForm, PageRequest pageRequest) {
		// TODO Auto-generated method stub

		QTbCust qTbCust = QTbCust.tbCust;
		Predicate predicate = null;

		predicate = StringUtils.isBlank(custForm.getCustId()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.id.custId.eq(custForm.getCustId()));
		predicate = StringUtils.isBlank(custForm.getCustLevel()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.custLevel.eq(custForm.getCustLevel()));
		predicate = StringUtils.isBlank(custForm.getEngName()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.engName.eq(custForm.getEngName()));
		predicate = StringUtils.isBlank(custForm.getLocName()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.locName.eq(custForm.getLocName()));
		predicate = StringUtils.isBlank(custForm.getPersonType()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.personType.eq(custForm.getPersonType()));
		predicate = StringUtils.isBlank(custForm.getRegionId()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.id.regionId.eq(custForm.getRegionId()));

		return tbCustHRespository.findAll(predicate, pageRequest);
	}

	@Autowired
	EntityManager entityManager;

	@Override
	public PageResult<List<CustDto>> queryDsl(CustForm custForm, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		log.info("****************************************************************************");
		QTbCust qTbCust = QTbCust.tbCust;
		QTbRegion qTbRegion = QTbRegion.tbRegion;

		Predicate predicate = null;

		PageResult<List<CustDto>> pr = new PageResult<List<CustDto>>();

		predicate = StringUtils.isBlank(custForm.getCustId()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.id.custId.eq(custForm.getCustId()));
		predicate = StringUtils.isBlank(custForm.getCustLevel()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.custLevel.eq(custForm.getCustLevel()));
		predicate = StringUtils.isBlank(custForm.getEngName()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.engName.eq(custForm.getEngName()));
		predicate = StringUtils.isBlank(custForm.getLocName()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.locName.eq(custForm.getLocName()));
		predicate = StringUtils.isBlank(custForm.getPersonType()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.personType.eq(custForm.getPersonType()));
		predicate = StringUtils.isBlank(custForm.getRegionId()) ? predicate
				: ExpressionUtils.and(predicate, qTbCust.id.regionId.eq(custForm.getRegionId()));

		JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

		QueryResults<Tuple> qr = queryFactory
				.select(qTbCust.id.custId, qTbRegion.regionName, qTbCust.engName, qTbCust.locName, qTbCust.custLevel,
						qTbCust.personType)
				.from(qTbCust).leftJoin(qTbRegion)
				.on(Expressions.stringOperation(Ops.UPPER, qTbCust.id.regionId).eq(qTbRegion.regionId)).where(predicate)
				.offset(pageRequest.getOffset()).limit(pageRequest.getPageSize()).fetchResults();

		List<CustDto> result = qr.getResults().stream()
				.map(tuple -> CustDto.builder().custId(tuple.get(qTbRegion.regionName))
						.regionId(tuple.get(qTbCust.id.custId)).engName(tuple.get(qTbCust.engName))
						.locName(tuple.get(qTbCust.locName)).custLevel(tuple.get(qTbCust.custLevel))
						.personType(tuple.get(qTbCust.personType)).build())
				.collect(Collectors.toList());
				
		pr.setData(result);
		pr.setTotal(qr.getTotal());
		pr.setPageSize(qr.getLimit());
		pr.setCurrent(qr.getOffset()+1);
		
		if(qr.getTotal()>0) {
			pr.setStatus(HttpStatus.ok);
			pr.setMessage("查詢成功!");
		}else {
			pr.setStatus(HttpStatus.empty);
			pr.setMessage("查無資料!");
		}
		
		
		return pr;
	}

}
