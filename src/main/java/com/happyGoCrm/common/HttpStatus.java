package com.happyGoCrm.common;

public class HttpStatus {
	public final static String ok="ok";
	public final static String empty="empty";
	public final static String fail="fail";
	public final static String valid="valid";
	public final static String exception="exception";
}
