package com.happyGoCrm.model.dto.cust;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@Data
@ToString
public class CustDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6685809659261168001L;

	private String custId;
	private String regionId;
	private String personType;
	private String locName;
	private String engName;
	private String custLevel;
	
}
