package com.happyGoCrm.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@EqualsAndHashCode
public class TbRegion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4543596005540523058L;

	@Id
	@Column(name="REGION_ID")
	private String regionId;
	
	@Column(name="REGION_Name")
	private String regionName;
}
