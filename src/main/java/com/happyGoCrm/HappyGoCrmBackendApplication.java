package com.happyGoCrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HappyGoCrmBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HappyGoCrmBackendApplication.class, args);
	}

}
