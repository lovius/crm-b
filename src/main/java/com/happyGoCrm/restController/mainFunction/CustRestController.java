package com.happyGoCrm.restController.mainFunction;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.happyGoCrm.common.HttpStatus;
import com.happyGoCrm.common.PageResult;
import com.happyGoCrm.common.Result;
import com.happyGoCrm.exception.SqlConditionItemException;
import com.happyGoCrm.model.converter.CustConverter;
import com.happyGoCrm.model.dto.cust.CustDto;
import com.happyGoCrm.model.entity.TbCust;
import com.happyGoCrm.model.queryForm.cust.CustForm;
import com.happyGoCrm.service.cust.CustService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/cust")
@Slf4j
public class CustRestController {

	@Autowired
	private CustService tbCustService;

	@Autowired
	private CustConverter tbCustConverter;

	@PostMapping("/add")
	public ResponseEntity<Result<String>> save(@RequestBody @Valid TbCust tbCust, BindingResult br) throws Exception {

		log.info("tbCust=" + tbCust.toString());

		if (br.hasErrors()) {
			StringBuilder sb = new StringBuilder();
			for (ObjectError index : br.getAllErrors()) {
				sb.append(index.getDefaultMessage() + "<br />");
			}
			return ResponseEntity.ok(new Result<String>(HttpStatus.valid, null, sb.toString()));
		}

		/*
		 * long checkPersonID = tbCustService.checkPersonID(tbCust.getPersonId()); if(
		 * checkPersonID >0 ) { return ResponseEntity.ok(new
		 * Result<String>(HttpStatus.valid,null,"身分證已存在!")); }
		 */

		try {
			TbCust tbCustResult = tbCustService.saveAndGetPk(tbCust);
			log.info(tbCustResult.getId().getCustId());
			return ResponseEntity.ok(new Result<String>(HttpStatus.ok, tbCustResult.getId().getCustId()));
		} catch (Exception e) {
			throw new Exception("新增失敗!");
		}

	}

	// mybatis
	@GetMapping("/lists")
	public ResponseEntity<?> mybatis(CustForm tbCust, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) throws SqlConditionItemException {

		List<CustDto> tbCustDtoList = new ArrayList<CustDto>();
		log.info("current = " + current);
		log.info("pageSize = " + pageSize);
		PageHelper.startPage(current, pageSize);
		com.github.pagehelper.Page<TbCust> tbCustPage = tbCustService.getPage(tbCust);

		log.info("size = " + tbCustPage.getResult().size());
		log.info("size = " + tbCustPage.toString());
		for (TbCust index : tbCustPage.getResult()) {
			CustDto tbCustDto = tbCustConverter.convert(index);
			tbCustDtoList.add(tbCustDto);
		}

		if (tbCustDtoList.size() == 0) {
			return ResponseEntity.ok(new Result<String>(HttpStatus.empty, "查無會員資料!"));
		} else {

			PageResult<List<CustDto>> pr = new PageResult<List<CustDto>>();
			pr.setStatus(HttpStatus.ok);
			pr.setData(tbCustDtoList);
			pr.setTotal(tbCustPage.getTotal());
			pr.setCurrent(tbCustPage.getPageNum());
			pr.setPageSize(tbCustPage.getPageSize());
			return ResponseEntity.ok(pr);
		}

	}

	@GetMapping("/list")
	public ResponseEntity<?> querydsl(CustForm custForm, @RequestParam(defaultValue = "1") int current,
			@RequestParam(defaultValue = "10") int pageSize) throws SqlConditionItemException {
		
		PageRequest pageRequest = new PageRequest(current-1,pageSize);

		List<CustDto> tbCustDtoList = new ArrayList<CustDto>();
		
		PageResult pr = tbCustService.queryDsl(custForm,pageRequest);
		
		return ResponseEntity.ok(pr);

	}

}
